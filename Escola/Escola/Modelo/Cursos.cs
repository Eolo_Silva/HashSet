﻿namespace Escola.Modelo
{
    using Modelo;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    public class Cursos
    {
        //atributo
        private ISet<Alunos> ConjuntoDeAlunos = new HashSet<Alunos>();
        //propriedades
        public string Nome { get; set; }
        public List<Disciplinas>Disciplina { get; set; }
        public IList<Alunos> Aluno
        {
            get
            {
                return new ReadOnlyCollection<Alunos>(ConjuntoDeAlunos.ToList());
            }
        }
        public Cursos(string nome)
        {
            Nome = nome;
            Disciplina = new List<Disciplinas>();
        }
        internal void AdicionaDisciplina(Disciplinas disciplinas)
        {
            Disciplina.Add(disciplinas);
        }
        internal void matricula(Alunos aluno)
        {
            ConjuntoDeAlunos.Add(aluno);
        }
        internal bool EstaMatriculado(Alunos aluno)
        {
            return ConjuntoDeAlunos.Contains(aluno);
        }
    }
}

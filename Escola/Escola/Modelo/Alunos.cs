﻿namespace Escola.Modelo
{
    public class Alunos
    {
        public string Nome { get; set;}
        public int Numero { get; set; }
        public Alunos (string nome, int numero)
        {
            Nome = nome;
            Numero = numero;
        }
        public override string ToString()
        {
            return $"Nome: {Nome} - Numero: {Numero}";
        }
        public override bool Equals(object obj)
        {
            Alunos outroAluno = obj as Alunos;

            if (outroAluno == null)
            {
                return false;
            }
            return this.Nome.Equals(outroAluno.Nome);
        }
        public override int GetHashCode()
        {
            return this.Nome.GetHashCode();
        }
    }
}

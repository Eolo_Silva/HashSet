﻿namespace Escola.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class Disciplinas
    {
        public string Titulo { get; set; }
        public int Tempo { get; set; }

        public Disciplinas(string titulo, int tempo)
        {
            Titulo = titulo;
            Tempo = tempo;
        }
        public override string ToString()
        {
            return $"Titulo: {Titulo} - Tempo: {Tempo} horas";
        }
    }
}

﻿namespace Escola
{
    using Modelo;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    class Program
    {
        static void Main(string[] args)
        {
            //declarar o curso
            Cursos tpsi = new Cursos("Programação de Sistemas de Informação");

            //Adiciona 3 Disciplinas
            tpsi.AdicionaDisciplina(new Disciplinas("Algoritmos", 25));
            tpsi.AdicionaDisciplina(new Disciplinas("Estruturada", 50));
            tpsi.AdicionaDisciplina(new Disciplinas("POO", 30));

            foreach (var disciplinas in tpsi.Disciplina)
            {
                Console.WriteLine(disciplinas); //o método override faz aparecer a informação
            }

            //criar os alunos com nome e matricula
            Alunos a1 = new Alunos("Vanessa Tires", 17889);
            Alunos a2 = new Alunos("Ana Luisa", 17454);
            Alunos a3 = new Alunos("Miguel Saraiva", 15323);

            //matricular os alunos no curso
            tpsi.matricula(a1);
            tpsi.matricula(a2);
            tpsi.matricula(a3);

            Console.WriteLine("Alunos Matriculados :");
            foreach (var aluno in tpsi.Aluno)
            {
                Console.WriteLine(aluno);
            }

            //A Vanessa está matriculada?
            Console.WriteLine(tpsi.EstaMatriculado(a1));

            Alunos outroAluno = new Alunos("Vanessa Tires", 17889);
            Console.WriteLine(tpsi.EstaMatriculado(outroAluno));
            Console.WriteLine(a1 == outroAluno);
            Console.WriteLine(a1.Equals(outroAluno));

            Console.ReadKey();
        }
    }
}
